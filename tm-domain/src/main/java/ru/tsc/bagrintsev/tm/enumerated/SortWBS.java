package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum SortWBS {

    BY_STATUS("Sort by status"),
    BY_NAME("Sort by name"),
    BY_CREATED("Sort by date created"),
    BY_STARTED("Sort by date started");

    @NotNull
    private final String displayName;

    SortWBS(
            @NotNull final String displayName
    ) {
        this.displayName = displayName;
    }

    @Nullable
    public static SortWBS toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return SortWBS.BY_CREATED;
        for (SortWBS sort : SortWBS.values()) {
            if (sort.toString().equals(value)) {
                return sort;
            }
        }
        return null;
    }

}
