package ru.tsc.bagrintsev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    private final String APPLICATION_VERSION = "buildNumber";

    @NotNull
    private final String AUTHOR_EMAIL = "email";

    @NotNull
    private final String AUTHOR_NAME = "developer";

    @NotNull
    @Value("#{environment['passwordHash.iterations']}")
    private Integer passwordHashIterations;

    @NotNull
    @Value("#{environment['passwordHash.keyLength']}")
    private Integer passwordHashKeyLength;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUserName;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databaseUserPassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.sql.dialect']}")
    private String databaseSqlDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl.auto']}")
    private String databaseHbm2DdlAuto;

    @NotNull
    @Value("#{environment['database.pool.min']}")
    private String databasePoolMinSize;

    @NotNull
    @Value("#{environment['database.pool.max']}")
    private String databasePoolMaxSize;

    @NotNull
    @Value("#{environment['database.pool.timeout']}")
    private String databasePoolTimeout;

    @NotNull
    @Value("#{environment['database.show.sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.format.sql']}")
    private String databaseFormatSql;

    @NotNull
    @Value("#{environment['database.use.sql.comments']}")
    private String databaseUseSqlComments;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLevelCache;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheProvConfigFileResPath;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactoryClass;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseCacheUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseCacheUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.hazelcast.use_lite_member']}")
    private String databaseCacheHazelcastUseLiteMember;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @NotNull
    @Override
    public String getApplicationVersion() {
        try {
            @NotNull final String version = Manifests.read(APPLICATION_VERSION);
            return version;
        } catch (IllegalArgumentException e) {
            return "1.31.test";
        }
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        try {
            @NotNull final String email = Manifests.read(AUTHOR_EMAIL);
            return email;
        } catch (IllegalArgumentException e) {
            return "testEmail";
        }
    }

    @NotNull
    @Override
    public String getAuthorName() {
        try {
            @NotNull final String name = Manifests.read(AUTHOR_NAME);
            return name;
        } catch (IllegalArgumentException e) {
            return "testName";
        }
    }

}
