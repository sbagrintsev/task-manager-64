package ru.tsc.bagrintsev.tm.api.sevice.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.TaskDto;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws IdIsEmptyException;

    @NotNull
    TaskDto setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final String projectId
    ) throws TaskNotFoundException;

}
