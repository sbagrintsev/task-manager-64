<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1 style="margin-top: 20px;">PROJECT EDIT</h1>

<%--@elvariable id="form" type="ru.tsc.bagrintsev.tm.model.FormDTO"--%>
<form:form action="/project/edit/${form.project.id}/" method="POST" modelAttribute="form">

    <form:hidden path="project.id"/>

    <form:hidden path="referer"/>

    <p>
        <form:label path="project.name">Name:</form:label>
        <form:input path="project.name"/>
    </p>

    <p>
        <form:label path="project.description">Description:</form:label>
        <form:input path="project.description"/>
    </p>

    <p>
        <form:label path="project.status">Status:</form:label>
        <form:select path="project.status">
            <form:option value="${null}" label="---//---"/>
            <form:options items="${form.statuses}" itemLabel="displayName"/>
        </form:select>
    </p>

    <p>
        <form:label path="project.dateStarted">Started:</form:label>
        <form:input type="date" path="project.dateStarted"/>
    </p>

    <p>
        <form:label path="project.dateFinished">Finished:</form:label>
        <form:input type="date" path="project.dateFinished"/>
    </p>

    <form:button name="SAVE">SAVE</form:button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>