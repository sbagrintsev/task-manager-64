package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.model.FormDTO;
import ru.tsc.bagrintsev.tm.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;

@Controller
@RequiredArgsConstructor
public class ProjectController {

    private final ProjectRepository projectRepository;

    @PostMapping("/project/create")
    public String create(@ModelAttribute("project") final FormDTO form) {
        projectRepository.save(form.getProject());
        return "redirect:" + form.getReferer();
    }

    @GetMapping("/project/create")
    public ModelAndView create() {
        final ProjectDTO project = new ProjectDTO();
        final FormDTO form = new FormDTO();
        form.setProject(project);
        form.setReferer("/projects");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("form", form);
        return modelAndView;
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") final FormDTO form) {
        projectRepository.save(form.getProject());
        return "redirect:" + form.getReferer();
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @RequestHeader String referer,
            @PathVariable("id") String id
    ) {
        final ProjectDTO project = projectRepository.findById(id);
        final FormDTO form = new FormDTO();
        form.setProject(project);
        form.setReferer(referer);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("form", form);
        return modelAndView;
    }

}
