package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;

@Controller
@RequiredArgsConstructor
public class TasksController {

    private final TaskRepository taskRepository;

    private final ProjectRepository projectRepository;

    @RequestMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskRepository.findAll());
        modelAndView.addObject("projects", projectRepository.projects);
        return modelAndView;
    }

}
