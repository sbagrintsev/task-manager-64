package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;

@Controller
@RequiredArgsConstructor
public class ProjectsController {

    private final ProjectRepository projectRepository;

    @RequestMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

}
