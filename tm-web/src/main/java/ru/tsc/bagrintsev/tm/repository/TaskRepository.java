package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.model.TaskDTO;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private final Map<String, TaskDTO> tasks = new LinkedHashMap<>();

    private long count = 1;

    {
        add("Demo");
        add("Test");
        add("Mega");
        add("Beta");
    }

    public void add(String name) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        tasks.put(task.getId(), task);
        count++;
    }

    public Collection<TaskDTO> findAll() {
        return tasks.values();
    }

    public TaskDTO findById(String id) {
        return tasks.get(id);
    }

    public void removeById(String id) {
        tasks.remove(id);
    }

    public void save(TaskDTO task) {
        tasks.put(task.getId(), task);
    }

}
