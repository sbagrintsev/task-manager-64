package ru.tsc.bagrintsev.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.bagrintsev.tm.enumerated.Status;

@Getter
@Setter
public class FormDTO {

    private final Status[] statuses = Status.values();

    private String referer;

    private ProjectDTO project;

    private TaskDTO task;

}
