package ru.tsc.bagrintsev.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.bagrintsev.tm")
public class ApplicationConfiguration {

}
