package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.model.FormDTO;
import ru.tsc.bagrintsev.tm.model.TaskDTO;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final TaskRepository taskRepository;

    private final ProjectRepository projectRepository;

    @PostMapping("/task/create")
    public String create(@ModelAttribute("task") final FormDTO form) {
        taskRepository.save(form.getTask());
        return "redirect:" + form.getReferer();
    }

    @GetMapping("/task/create")
    public ModelAndView create() {
        final TaskDTO task = new TaskDTO();
        final FormDTO form = new FormDTO();
        form.setTask(task);
        form.setReferer("/tasks");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("form", form);
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") final FormDTO form) {
        taskRepository.save(form.getTask());
        return "redirect:" + form.getReferer();
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @RequestHeader String referer,
            @PathVariable("id") String id
    ) {
        final TaskDTO task = taskRepository.findById(id);
        final FormDTO form = new FormDTO();
        form.setTask(task);
        form.setReferer(referer);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("form", form);
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

}
